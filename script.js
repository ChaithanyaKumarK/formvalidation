const form = document.getElementById("form");
const err_name = document.getElementById("user_name_error");
const err_email = document.getElementById("email_error");
const err_password = document.getElementById("password_error");
const err_re_password = document.getElementById("re-password_error");

form.addEventListener("submit", (event1) => {
  event1.preventDefault();
  checkData();
});

function checkData() {
  const name = document.getElementById("user_name");
  const email = document.getElementById("email");
  const password = document.getElementById("password");
  const re_password = document.getElementById("re-password");
  let flag = 0;

  //validation for user name
  if (name.value == "") {
    setError(name, "username should not be empty");
  } else {
    setSucess(name);
    flag++;
  }

  //validation for email
  if (email.value == "") {
    setError(email, "email should not be empty");
  } else if (
    !/^([a-z\d\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/.test(
      email.value
    )
  ) {
    setError(email, "enter a valid email");
  } else {
    setSucess(email);
    flag++;
  }

  //  validation for password
  if (password.value == "") {
    setError(password, "password should not be empty");
  } else {
    setSucess(password);
    flag++;
  }

  // validation for password check
  if (password.value == "") {
    setError(re_password, "password should not be empty");
  } else if (re_password.value !== password.value) {
    setError(re_password, "passwords doesn't match");
  } else {
    setSucess(re_password);
    flag++;
  }

  //reload after signup
  if (flag > 3) {
    location.reload();
  }
}

function setSucess(element) {
  const form_element = element.parentElement;
  form_element.className = "form_element success";
  form_element.querySelector("span").innerText = "";
}

function setError(element, message) {
  const form_element = element.parentElement;
  form_element.className = "form_element error";
  form_element.querySelector("span").innerText = message;
}
